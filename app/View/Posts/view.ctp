<article class="row format-standard">
<div class="s-content__header col-full">
    <h1 class="s-content__header-title">
        <?php echo $post['Post']['title']; ?>
    </h1>
    <ul class="s-content__header-meta">
        <li class="date"><?php echo $this->Time->format('F jS, Y h:i A', $post['Post']['created_at'])?>
        <li class="cat">
            In
            <a href="#0">Lifestyle</a>
            <a href="#0">Travel</a>
        </li>
    </ul>
</div>
<div class="s-content__media col-full">
    <?php if(!empty($post['Post']['post_pic'])) : ?>
        <div class="s-content__post-thumb"><?=$this->Html->image($post['Post']['post_pic'], array('class'=>'center'))?></div>
    <?php endif; ?>
</div>
<div class="col-full s-content__main">
    <?php echo $post['Post']['body']; ?>
    <p class="s-content__tags">
        <span>Post Tags</span>
        <span class="s-content__tag-list">
            <a href="#0">orci</a>
            <a href="#0">lectus</a>
            <a href="#0">varius</a>
            <a href="#0">turpis</a>
        </span>
    </p>
    <div class="s-content__author">
    <?php echo $this->Html->image($post['User']['profile_pic'], array('class'=>'profile-user-img img-responsive img-circle', 'alt'=>'User Image')); ?>
        <div class="s-content__author-about">
            <h4 class="s-content__author-name">
                <a href="#0"><?php echo $post['User']['username']; ?></a>
            </h4>
            <p><i>Bio here soon!</i></p>
            <ul class="s-content__author-social">
               <li><a href="#0">Facebook</a></li>
               <li><a href="#0">Twitter</a></li>
               <li><a href="#0">GooglePlus</a></li>
               <li><a href="#0">Instagram</a></li>
            </ul>
        </div>
    </div>
    <div class="s-content__pagenav">
        <div class="s-content__nav">
            <div class="s-content__prev">
                <a href="#0" rel="prev">
                    <span>Previous Post</span>
                    Tips on Minimalist Design 
                </a>
            </div>
            <div class="s-content__next">
                <a href="#0" rel="next">
                    <span>Next Post</span>
                    Less Is More 
                </a>
            </div>
        </div>
    </div>
</div>
</article>
<div class="comments-wrap">
<div id="comments" class="row">
    <div class="col-full">
        <h3 class="h2">5 Comments</h3>
        <ol class="commentlist">
            <?php foreach ($comments as $comment) : ?>
            <li class="depth-1 comment">
                <div class="comment__avatar">
                    <?= $this->Html->image($comment['User']['profile_pic'], array('width'=>50, 'height'=>50, 'class'=>'avatar')); ?>
                </div>
                <div class="comment__content">
                    <div class="comment__info">
                        <cite><?= $comment['User']['username']?></cite>
                        <div class="comment__meta">
                            <time class="comment__time"><?= $this->Time->format('F jS, Y h:i A', $comment['Comment']['created_at'])?></time>
                        </div>
                    </div>
                    <div class="comment__text">
                    <p><?= $comment['Comment']['body']?></p>
                    </div>
                </div>
            </li>
            <?php endforeach; ?>
        </ol>
        <div class="respond">
            <h3 class="h2">Add Comment</h3>
            <?php if(!$authUser) : ?>
            <fieldset>
                <?php echo $this->Form->create('Comment'); ?>
                <div class="form-field">
                    <?php echo $this->Form->input('author', array('label'=>false, 'class'=>'full-width','placeholder'=>'Name')); ?>
                </div>
                <div class="form-field">
                    <?php echo $this->Form->input('email', array('label'=>false, 'class'=>'full-width', 'placeholder'=>'Email Address')); ?>
                </div>
                <div class="message form-field">
                    <?php echo $this->Form->input('body', array('label'=>false, 'class'=>'full-width', 'type'=>'text', 'placeholder'=>'Comment')); ?>
                </div>
                <?php echo $this->Form->input('post_id', array('type'=>'hidden', 'value'=>$post['Post']['id'])); ?>
                <?php echo $this->Form->end('Comment', array('type'=>'submit', 'class'=>'submit btn--primary btn--large full-width')); ?>
            </fieldset>
            <?php else : ?>
            <fieldset>
                <?php echo $this->Form->create('Comment'); ?>
                <div class="message form-field">
                    <?php echo $this->Form->input('body', array('label'=>false, 'class'=>'full-width', 'type'=>'text', 'placeholder'=>'Comment')); ?>
                </div>
                <?php echo $this->Form->input('post_id', array('type'=>'hidden', 'value'=>$post['Post']['id'])); ?>
                <?php echo $this->Form->end('Comment', array('type'=>'submit', 'class'=>'submit btn--primary btn--large full-width')); ?>
            </fieldset>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
