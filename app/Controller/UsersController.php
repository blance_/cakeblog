<?php
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {
	public $components = array('Session');
	public $helpers = array('Html', 'Form', 'Js');

	public function beforeFilter() {
		$this->Auth->allow('register');
	}

	public function index() {
		$this->set('title_for_layout', 'List of Users');
		$users = $this->User->find('all');
		$this->set('users',$users);
	}

	public function view($id=null) {
		if(!$id) {
			throw new NotFoundException(__('Invalid user'));
		}
		$user=$this->User->findById($id);
		if(!$user) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->set('user', $user);
	}

	public function register() {
		$this->set('title_for_layout', 'Register');
		$this->layout='login';
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$Email = new CakeEmail('gmail');
				$Email 	-> from(array('me@example.com' => 'MicroBlog CakePHP'))
						-> to($this->request->data['User']['email'])
						-> subject('Confirm your email address')
						-> emailFormat('html')
						-> template('sendmail', 'default')
						-> send();
				if($Email) {
					$this->Session->SetFlash('The user has been created, check your mail');
					$this->redirect('index');
				}
			}
		}
	}

	public function edit($id) {
		$user = $this->User->findById($id);
		if ($this->request->is(array('post', 'put'))) {
			$this->User->id=$id;
			if ($this->User->save($this->request->data)) {
				$this->Session->SetFlash('The user has been edited');
				$this->redirect('viewUsers');
			}
		}
		$this->request->data = $user;
	}

	public function delete($id) {
		$this->User->id=$id;
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->delete()) {
				$this->Session->SetFlash('The user has been deleted');
				$this->redirect('viewUsers');
			}
		}
	}

	public function login() {
		$title = $this->Post->field('title', array('Post.id' => $id));
		$this->set('title_for_layout', 'Login');
		$this->layout='login';
		if($this->request->is('post')) {
			if($this->Auth->login()) {
				return $this->redirect($this->Auth->redirectUrl());
			} else{
				$this->Session->setFlash('Invalid username or password');
			}
		}
	}

	public function logout() {
		$this->Auth->logout();
		$this->redirect('/users/login');
	}

	public function profile($id) {
		$this->set('title_for_layout', 'User Profile');
		$this->loadModel('Post');
		$this->loadModel('Like');
		$this->loadModel('Comment');
		$this->set('user', $this->User->findById($id));
		$this->set('posts', $this->Post->find('all', array('conditions'=>array('Post.user_id'=>$id))));
		$this->set('like', $this->Like->find('all'));

		if($this->request->is('ajax')) {
			pr($this->request->data['Comments']);
			die();
			$this->render('profile', 'ajax');
		}

		// if($this->request->is('post')){
		// 	$this->Comment->create();
			
		// 	//dito ako maglalagay ng logic pag magccomment si guest
		// 	$this->request->data['Comment']['user_id'] = AuthComponent::user('id');
		// 	if($this->Comment->save($this->request->data)) {
		// 		$this->Session->SetFlash('The comment has been created');
		// 		$this->redirect(array('controller'=>'posts', 'action'=>'timeline'));
		// 	}
		// 	$this->Flash->error(__('Unable to add your comment'));
		// }
	}
}
