<div class="row masonry-wrap">
    <div class="masonry">
        <div class="grid-sizer"></div>
        <?php foreach ($posts as $post) : ?>
            <article class="masonry__brick entry format-standard">
                <?php if(!empty($post['Post']['post_pic'])) : ?>
                    <div class="entry__thumb">
                        <?=$this->Html->image($post['Post']['post_pic'], array('height'=>600, 'width'=>400, 'class'=>'entry__thumb-link'))?>
                    </div>
                <?php endif; ?>
                <div class="entry__text">
                    <div class="entry__header">
                        <div class="entry__date">
                            <a href="#"><?php $this->Time->format('F jS, Y h:i A', $post['Post']['created_at'])?></a>
                        </div>
                        <h1 class="entry__title"><?php echo $this->Html->link($post['Post']['title'], array('controller'=>'posts', 'action'=>'view', $post['Post']['id']))?></h1>
                    </div>
                    <div class="entry__excerpt">
                        <p><?= $post['Post']['body']?></p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="#">Health</a>
                        </span>
                    </div>
                </div>
            </article>
        <?php endforeach; ?>
    </div>
</div>
<div class="row">
    <div class="col-full">
        <nav class="pgn">
            <ul>
                <li><?php  $paginate = $this->Paginator;
                     echo $paginate->first('First', array('class'=>'pgn__num'))?></li>
                <?php if ($paginate->hasPrev()) : ?>
                    <li><?php echo $paginate->prev('<<')?></li>
                <?php endif; ?>
                <li><?php echo $paginate->numbers(array('modulus'=>2, 'class'=>'pgn__num'))?></li>
                <?php if ($paginate->hasNext()) : ?>
                    <li><?php echo $paginate->next('>>')?></li>
                <?php endif; ?>
                <li><?php echo $paginate->last('Last', array('class'=>'pgn__num'))?></li>
            </ul>
        </nav>
    </div>
</div>
