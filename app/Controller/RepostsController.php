<?php

class RepostsController extends AppController {
    public function repost($postid) {
        $userid=AuthComponent::user('id');
        if($this->request->is(array('post'))) {
            $this->Repost->save(array('post_id'=>$postid, 'user_id'=>$userid));
            $this->Session->SetFlash(__('Reposted'));
            $this->redirect(array('controller'=>'posts', 'action'=>'timeline'));
        }
    }
}