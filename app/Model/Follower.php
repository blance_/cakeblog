<?php
App::uses('AppModel', 'Model');
/**
 * Follower Model
 *
 * @property User $User
 * @property Follower $Follower
 * @property Follower $Follower
 */
class Follower extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'follower_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'created_at' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'deleted_at' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

// /**
//  * belongsTo associations
//  *
//  * @var array
//  */
// 	public $belongsTo = array(
// 		'User' => array(
// 			'className' => 'User',
// 			'foreignKey' => 'user_id',
// 			'conditions' => '',
// 			'fields' => '',
// 			'order' => ''
// 		),
// 		'Follower' => array(
// 			'className' => 'Follower',
// 			'foreignKey' => 'follower_id',
// 			'conditions' => '',
// 			'fields' => '',
// 			'order' => ''
// 		)
// 	);

// /**
//  * hasMany associations
//  *
//  * @var array
//  */
// public $hasMany = array(
// 	'Follower' => array(
// 		'className' => 'Follower',
// 		'foreignKey' => 'follower_id',
// 		'dependent' => false,
// 		'conditions' => '',
// 		'fields' => '',
// 		'order' => '',
// 		'limit' => '',
// 		'offset' => '',
// 		'exclusive' => '',
// 		'finderQuery' => '',
// 		'counterQuery' => ''
// 	)
// );

}
