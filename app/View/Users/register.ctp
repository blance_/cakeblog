<p class="login-box-msg">Create an account</p>
<?php echo $this->Form->create('User'); ?>
  <div class="form-group has-feedback">
	<?php echo $this->Form->input('firstname', array('class'=>'form-control', 'placeholder'=>'First Name', 'label'=>false));?>
	<span class="glyphicon glyphicon-user form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
	<?php echo $this->Form->input('lastname', array('class'=>'form-control', 'placeholder'=>'Last Name', 'label'=>false));?>
	<span class="glyphicon glyphicon-user form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
	<?php echo $this->Form->input('username', array('class'=>'form-control', 'placeholder'=>'Username', 'label'=>false));?>
	<span class="glyphicon glyphicon-user form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
	<?php echo $this->Form->input('email', array('class'=>'form-control', 'placeholder'=>'Email Address', 'label'=>false));?>
	<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
	<?php echo $this->Form->input('password', array('class'=>'form-control', 'placeholder'=>'Password', 'label'=>false));?>
	<span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
  <?php echo $this->Form->input('confirmpassword', array('type'=>'password', 'class'=>'form-control', 'placeholder'=>'Confirm Password', 'label'=>false));?>
	<span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="row">
	<div class="col-xs-4">
		<?php echo $this->Form->submit('Register', array('class'=>'btn btn-success btn-block btn-flat')); ?>
	</div>
	<div class="col-xs-12">
		<br>
		<?php echo $this->Html->link('I already have an account', array('controller'=>'users','action' => 'login')) ?>
	</div>
  </div>

