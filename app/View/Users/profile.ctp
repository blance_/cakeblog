<section class="content-header">
    <h1>
    User Profile
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">User profile</li>
    </ol>
</section>
<section class="content">
    <?php echo $this->Html->link('Profile Settings', array('controller'=>'users', 'action'=>'edit', $user['User']['id']), array('class'=>'btn btn-primary'))?><br><br>
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <?php echo $this->Html->image($user['User']['profile_pic'], array('class'=>'profile-user-img img-responsive img-circle', 'alt'=>'User Image')); ?>
                    <h3 class="profile-username text-center"><?=$user['User']['firstname'].' '.$user['User']['lastname']?></h3>
                    <p class="text-muted text-center"><?='@'. $user['User']['username']. '&nbsp';
                        if($user['User']['verified']==1) {
                        echo $this->Html->image('verified.png', array('alt'=>'Verified User', 'height'=>20, 'width'=>20, 'title'=>'Verified')); 
                    }
                    ?></p>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Followers</b> <?= $this->Html->link('Followers', array('controller' => 'followers', 'action' => 'showFollowers', $user['User']['id']), array('class'=>'pull-right')); ?>
                        </li>
                        <li class="list-group-item">
                            <b>Following</b> <a class="pull-right">0</a>
                        </li>
                        <li class="list-group-item">
                            <b>Friends</b> <a class="pull-right">0</a>
                        </li>
                    </ul>
                    <!-- lalagay ng if kung nafollow na -->
                    <?= $this->Form->postlink('Follow', array('controller' => 'followers', 'action' => 'follow', $user['User']['id']), array('class'=>'btn btn-primary btn-block')); ?>
                    <?= $this->Form->postlink('Unfollow', array('controller' => 'followers', 'action' => 'unfollow', $user['User']['id']), array('class'=>'btn btn-warning btn-block')); ?>
                </div>
            </div>
            <div class="box box-primary">
                <!-- Profile setting customization ... later -->
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>
                <div class="box-body">
                    <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
                    <p class="text-muted">Edit your education</p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                    <p class="text-muted">Edit your location</p>
                    <hr>
                    <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                    <hr>
                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                    <p>Insert notes here</p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">Posts</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="activity">
                    <?php if(!empty($posts)) :
                        foreach($posts as $post) : ?>
                        <div class="post">
                            <div class="user-block">
                            <?php echo $this->Html->image($user['User']['profile_pic'], array('class'=>'img-circle img-bordered-sm', 'alt'=>'User Image')); ?>
                                <span class="username">
                                    <a href="#"><?php echo $post['User']['username']?></a>
                                </span>
                            <span class="description"><?php echo $post['Post']['created_at']?></span>
                            </div>
                            <h4><b><?php echo $post['Post']['title'] ?></b></h4>
                            <p><?php echo $post['Post']['body'] ?></p>
                            <p>Post ID: <?php echo $post['Post']['id'] ?></p>
                            <ul class="list-inline">
                                </li><i class="fa fa-share margin-r-5"></i><?= $this->Form->postlink('Repost', array('controller' => 'reposts', 'action' => 'repost', $post['Post']['id']), array('class'=>'link-black text-sm')); ?></li>
                                <!-- lalagyan ng if kung nalike na -->
                                <li><i class="fa fa-thumbs-o-up margin-r-5"></i><?= $this->Form->postlink('Like', array('controller' => 'likes', 'action' => 'like', $post['Post']['id'], $user['User']['id']), array('class'=>'link-black text-sm')); ?></li>
                                <li><i class="fa fa-thumbs-o-down margin-r-5"></i><?= $this->Form->postlink('Unlike', array('controller' => 'likes', 'action' => 'unlike'), array('class'=>'link-black text-sm')); ?></li>
                                </li>
                                <li class="pull-right"><a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments (0)</a></li>
                            </ul>
                            <hr>
                            <?php foreach ($post['Comment'] as $comment) : ?>
                            <div class='post clearfix' id="commentStatus">
                                <div class='user-block'>
                                    <li style='list-style-type: none;' class='dropdown pull-right'>
                                        <a href='' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-bars'></i> <span class='caret'></span></a>
                                        <ul class='dropdown-menu' role='menu'>
                                            <li><?= $this->HTML->link('Edit', array('controller'=>'posts', 'action'=>'editComment', $comment['id']), array('class'=>'btn btn-link'));?></li>
                                            <li><?= $this->Form->postlink('Delete', array('controller'=>'posts', 'action'=>'deleteComment', $comment['id']), array('confirm'=>'Are you sure you want to delete this comment?', 'class'=>'btn btn-link'));?></li>
                                        </ul>
                                    </li><input type='hidden' id='uid' class='form-control'>
                                    <?php 
                                        $image=$comment['profile_pic'];
                                        echo $this->Html->image($image, array('class'=>'img-circle img-bordered-sm', 'alt'=>'User Image')); 
                                    ?>
                                    <span class='username'><a href=''><?php echo $comment['username']?></a></span>
                                    <span class='description'><?php echo $comment['created_at']?></span>
                                </div>
                                    <p data-target='combody'><?php echo $comment['body']?></p>
                            </div>
                        <?php endforeach; ?>
                            <?php   
                                // echo $this->Form->create('Comment',  array('default'=>false));
                                echo $this->Form->create('Comment');
                                echo $this->Form->input('Comment.body', array('label'=>false, 'class'=>'form-control input-sm', 'type'=>'text'));
                                echo $this->Form->input('Comment.post_id', array('type'=>'hidden', 'value'=>$post['Post']['id']));
                                echo $this->Form->end('Comment');
                            ?>
                        </div>
                    <?php endforeach; else :?>
                        <p> No posts to show</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
    //papalitan ng pure ajax
    $data = $this->Js->get('#CommentsCommentForm')->serializeForm(array('isForm'=>true,'inline'=>true));
    $this->Js->get('#CommentForm')->event(
        'submit',
        $this->Js->request(
            array('controller'=>'users', 'action'=>'profile'),
            array(
                'update'=>'#commentSection',
                'data' =>$data,
                'async'=>true,
                'dataExpression'=>true,
                'method'=>'POST'
            )
        )
    );
    echo $this->Js->writeBuffer();
?>
