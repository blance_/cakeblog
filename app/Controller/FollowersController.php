<?php 
class FollowersController extends AppController {

    public function follow($userid){
        $this->loadModel('User');
        $followerid=AuthComponent::user('id');
        if($this->request->is(array('post'))) {
            if($followerid==1) {
                $this->Follower->save(array('follower_id'=>$followerid, 'user_id'=>$userid));
                $this->User->updateAll(array('verified'=>1));
                $this->Session->SetFlash(__('Followed user'));
                $this->redirect(array('controller'=>'users', 'action'=>'profile', $userid));
            } else {
                $this->Follower->save(array('follower_id'=>$followerid, 'user_id'=>$userid));
                $this->Session->SetFlash(__('Followed user'));
                $this->redirect(array('controller'=>'users', 'action'=>'profile', $userid));
            }
        } else {
            $this->Session->SetFlash(__('Unable to follow'));
        }
    }

    // public function unfollow($userid){
    //     $this->loadModel('User');
    //     $followerid=AuthComponent::user('id');
    //     if($this->request->is(array('post'))) {
    //         $id=$this->Follower->find('first', array('fields'=>'id', 'conditions' => array('follower_id' => $followerid, 'user_id'=>$userid)));
    //         $this->Follower->deleteAll($id);
    //         //$this->redirect(array('controller'=>'users', 'action'=>'profile', $userid));
    //         //if(AuthComponent::user('id')==1) {
    //             // $this->User->updateAll(array('verified'=>0));
                
              
    //             // $this->Follower->deleteAll($id);
    //             // $this->Session->SetFlash(__('Unfollowed user'));
               
    //         //} 
    //         // else {
    //         //     $this->Follower->deleteAll($id);
    //         //     $this->Session->SetFlash(__('Unfollowed user'));
    //         // }
    //     } else {
    //         $this->Session->SetFlash(__('Unable to unfollow'));
    //     }
    // }

    public function showFollowing($userid){
        $following=$this->Follower->find('all', array('conditions'=>array('follower_id'=>$userid)));
        pr($following);
        die();
    }

    public function showFollowers($userid) {
        $followers=$this->Follower->find('all', array('conditions'=>array('user_id'=>$userid)));
        pr($followers);
        die();
    }

}
?>