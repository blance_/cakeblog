<?php 
	echo $this->Html->script(array('bower_components/jquery/dist/jquery.min.js', 
	'bower_components/jquery-ui/jquery-ui.min.js',
	'bower_components/raphael/raphael.min.js',
	'bower_components/morris.js/morris.min.js',
	'bower_components/jquery-sparkline/dist/jquery.sparkline.min.js',
	'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
	'plugins/jvectormap/jquery-jvectormap-world-mill-en.js', 
	'bower_components/jquery-knob/dist/jquery.knob.min.js', 
	'bower_components/moment/min/moment.min.js',
	'bower_components/bootstrap-daterangepicker/daterangepicker.js',
	'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
	'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
	'bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
	'bower_components/fastclick/lib/fastclick.js',
	'adminlte.min.js',
	'pages/dashboard.js',
	'demo.js',
	'js_debug_toolbar.js', 
	'bower_components/bootstrap/js/dropdown.js',
	'bower_components/datatables.net/js/jquery.dataTables.min.js',
	'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
	'datatables.js'));
?>