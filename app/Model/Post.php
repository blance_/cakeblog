<?php
App::uses('AppModel', 'Model');

class Post extends AppModel {

	public $validate = array(
		'title' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
			'between' => array(
				'rule' => array('lengthBetween', 2, 50),
				'message' => 'Title should be at least 2 chars long',
				'required' => true,
			),
		),
		'body' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		'between' => array(
			'rule' => array('lengthBetween', 2, 140),
			'message' => 'Body should be at least 2 chars long',
			'required' => true,
		),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'created_at' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'deleted_at' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'category_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		)
	);

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'post_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Like' => array(
			'className' => 'Like',
			'foreignKey' => 'post_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Repost' => array(
			'className' => 'Repost',
			'foreignKey' => 'post_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'id',
			'dependent' => false
		)
	);

}
