<?php
/**
 * Follower Fixture
 */
class FollowerFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true),
		'follower_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true),
		'created_at' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'deleted_at' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'follower_id' => 1,
			'created_at' => '2018-07-19 06:26:57',
			'deleted_at' => '2018-07-19 06:26:57'
		),
	);

}
