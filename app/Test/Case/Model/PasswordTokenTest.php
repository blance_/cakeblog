<?php
App::uses('PasswordToken', 'Model');

/**
 * PasswordToken Test Case
 */
class PasswordTokenTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.password_token',
		'app.user',
		'app.comment',
		'app.follower',
		'app.login_token',
		'app.post',
		'app.like',
		'app.repost'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PasswordToken = ClassRegistry::init('PasswordToken');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PasswordToken);

		parent::tearDown();
	}

}
