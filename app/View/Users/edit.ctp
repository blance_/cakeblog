<h3>Edit information</h3>
<?php
	echo $this->Form->create('User');
	echo $this->Form->input('firstname');
	echo $this->Form->input('lastname');
	echo $this->Form->input('username');
	echo $this->Form->input('email');
	echo $this->Form->input('password');
	echo $this->Form->input('confirmpassword', array('type'=>'password'));
	echo $this->Form->end('Save');
?>
