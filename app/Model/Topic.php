<?php
App::uses('AppModel', 'Model');

class Topic extends AppModel {

	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
		'frequency' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
	);
}
