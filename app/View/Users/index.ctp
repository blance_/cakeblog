
<section class="content-header">
	<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">Users</li>
	</ol>
</section>

<?php echo $this->start('navigation'); ?>
	<li class="treeview active">
		<a href="#">
			<i class="fa fa-users"></i> <span>Users</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href=""><i class="fa fa-circle-o"></i>Administrator</a></li>
			<li><a href=""><i class="fa fa-circle-o"></i>Members</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-pencil"></i> <span>Posts</span>
		</a>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-calendar"></i> <span>Timeline</span>
		</a>
    </li>
<?php $this->end(); ?>

<section class="content">
	<?= $this->Html->link('Add New User', array('controller'=>'users','action' => 'register')) ?><br>
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">List of Users</h3>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-striped" id="example1">
				<thead>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Username</th>
						<th>Email Address</th>
						<th>Profile Picture</th>
						<th>Status</th>
						<th>View</th>
						<th>Edit</th>
						<th>Deactivate</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($users as $user) : ?>
					<tr>
						<td><?= $user['User']['firstname'];?></td>
						<td><?= $user['User']['lastname'];?></td>
						<td><?= $user['User']['username'];?></td>
						<td><?= $user['User']['email'];?></td>
						<td><?= $user['User']['profile_pic'];?></td>
						<td><?= $user['User']['email_status'];?></td>
						<td><?= $this->Html->link('View Profile', array('controller' => 'users', 'action' => 'profile', $user['User']['id'])); ?></td>
						<td><?= $this->HTML->link('Edit', array('controller'=>'users', 'action'=>'edit', $user['User']['id'])); ?></td>
						<td><?= $this->Form->postlink('Delete', array('controller'=>'users', 'action'=>'delete', $user['User']['id']), array('confirm'=>'Are you sure you want to delete this user?')); ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				<?php unset($user); ?>
			</table>
		</div>
	</div>
</section>
