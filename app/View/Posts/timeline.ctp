<section class="content">
    <?php foreach($posts as $post) : ?>
        <div class="box">
            <div class="box default">
                    <div class="post">
                        <div class="user-block">
                                <?php echo $this->Html->image($post['User']['profile_pic'], array('class'=>'img-circle img-bordered-sm', 'alt'=>'User Image')); ?>
                                <span class="username">
                                    <a href="#"><?php echo $post['User']['username']?></a>
                                </span>
                            <span class="description"><?php echo $post['Post']['created_at']?></span>
                        </div>
                        <h4><b><?php echo $post['Post']['title'] ?></b></h4>
                        <p><?php echo $post['Post']['body'] ?></p>
                        <p><?php echo 'id: '.$post['Post']['id'] ?></p>
                        <ul class="list-inline">
                            <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Repost</a></li>
                            <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a></li>
                            <li class="pull-right"><a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments (0)</a></li>
                        </ul>
                        <?php foreach ($post['Comment'] as $comment) : ?>
                            <div class='post clearfix' id="">
                                <?= $this->HTML->link('Edit', array('controller'=>'posts', 'action'=>'editComment', $comment['id']));?>
						        <?= $this->Form->postlink('Delete', array('controller'=>'posts', 'action'=>'deleteComment', $comment['id']), array('confirm'=>'Are you sure you want to delete this comment?'));?>
                                <div class='user-block'>
                                    <li style='list-style-type: none;' class='dropdown pull-right'>
                                        <a href='' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-bars'></i> <span class='caret'></span></a>
                                        <ul class='dropdown-menu' role='menu'>
                                            <li><a href class='btn btn-link' data-id="" data-role='comupdate' data-toggle='modal'>Edit</a></li>
                                            <li><a data-id="" id='btndeletecom' class='btn btn-link'>Delete</a></li>
                                        </ul>
                                    </li><input type='hidden' id='uid' class='form-control'>
                                    <?php 
                                        $image=$comment['profile_pic'];
                                        echo $this->Html->image($image, array('class'=>'img-circle img-bordered-sm', 'alt'=>'User Image')); 
                                    ?>
                                    <span class='username'><a href=''><?php echo $comment['username']?></a></span>
                                    <span class='description'><?php echo $comment['created_at']?></span>
                                </div>
                                    <p data-target='combody'><?php echo $comment['body']?></p>
                            </div>
                        <?php endforeach; ?>
                        <?php   
                            echo $this->Form->create('Comment');
                            echo $this->Form->input('Comment.body', array('label'=>false, 'class'=>'form-control input-sm', 'type'=>'text'));
                            echo $this->Form->input('Comment.post_id', array('type'=>'hidden', 'value'=>$post['Post']['id']));
                            echo $this->Form->end('Comment');
                        ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?> 
</section>