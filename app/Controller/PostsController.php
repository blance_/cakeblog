<?php
class PostsController extends AppController {

	public $components = array('Session', 'Flash', 'Paginator');
	public $helpers = array('Html', 'Time', 'Paginator');
	
	public function beforeFilter() {
		$this->Auth->allow('index', 'view');
	}
	
	public function listPost() {
		$this->set('posts', $this->Post->find('all'));
	}

	public function view($id = null) {
		$title = $this->Post->field('title', array('Post.id' => $id));
		$this->set('title_for_layout', $title);

		$this->set('authUser', AuthComponent::user('id'));
		$this->loadModel('Comment');
		$this->layout='user';
		if(!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		$post=$this->Post->findById($id);
		if(!$post){
			throw new NotFoundException(__('Invalid post'));
		}

		$this->set('post', $post);
		$this->set('comments', $this->Comment->find('all', array('conditions'=>array('post_id'=>$id))));

		if($this->request->is(array('post'))){
			$this->Comment->create();
			$this->request->data['Comment']['user_id'] = AuthComponent::user('id');
			if($this->Comment->save($this->request->data)) {
				$this->Session->SetFlash('Waiting for approval of your comment');
				$this->redirect('/posts/view/'. $id);
			}
			$this->Flash->error(__('Unable to add your comment'));
		}
	}

	public function timeline() {
		$posts = $this->Post->find('all');
		$this->loadModel('Comment');
		$this->set('posts', $this->Post->find('all'));
		
		if($this->request->is('post')){
			$this->Comment->create();
			//dito ako maglalagay ng logic pag magccomment si guest
			$this->request->data['Comment']['user_id'] = AuthComponent::user('id');
			if($this->Comment->save($this->request->data)) {
				$this->Session->SetFlash('The comment has been created');
				$this->redirect('timeline');
			}
			$this->Flash->error(__('Unable to add your comment'));
		}
	}

	public function add() {
		if($this->request->is('post')){
			$this->Post->create();
			if (!empty($this->request->data['Post']['upload']['name'])) {
				$file = $this->request->data['Post']['upload'];
				$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
				$arr_ext = array('jpg', 'jpeg', 'gif','png');
				if (in_array($ext, $arr_ext)) {
					move_uploaded_file($file['tmp_name'], WWW_ROOT .'img/'. $file['name']);
					$this->request->data['Post']['post_pic'] = $file['name'];
				}
			}
			$this->request->data['Post']['user_id'] = AuthComponent::user('id');
			if($this->Post->save($this->request->data)) {
				$this->Session->SetFlash('The post has been created');
				$this->redirect('index');
			}
			$this->Flash->error(__('Unable to add your post'));
		}
	}

	public function edit($id=null) {
		if(!$id){
			throw new NotFoundException(__('Invalid post'));
		}
		$post=$this->Post->findById($id);
		if(!$post) {
			throw new NotFoundException(__('Invalid post'));
		}
		if($this->request->is(array('post', 'put'))) {
			$this->Post->id = $id;
			if($this->Post->save($this->request->data)) {
				$this->Flash->success(__('The post has been updated'));
				return $this->redirect(array('action'=>'index'));
			}
			$this->Flash->error(__('Unable to update your post'));
		}
		if(!$this->request->data) {
			$this->request->data=$post;
		}
	}

	public function editComment($id=null) {
		$this->loadModel('Comment');
		if(!$id) {
			throw new NotFoundException(__('Invalid comment'));
		}
		$comment=$this->Comment->findById($id);
		if(!$comment) {
			throw new NotFoundException(__('Invalid comment'));
		}
		if($this->request->is(array('post', 'put'))){
			$this->Comment->id=$id;
			if($this->Comment->save($this->request->data)) {
				$this->Flash->success(__('The comment has been updated'));
				return $this->redirect(array('action'=>'timeline'));
			}
			$this->Flash->error(__('Unable to update your comment'));
		}
		if(!$this->request->data) {
			$this->request->data=$comment;
		}
	}

	public function delete($id) {
		if($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		if($this->Post->delete($id)) {
			$this->Flash->success(__('The post has been deleted'));
		} else {
			$this->Flash->error(__('The post could not been deleted'));
		}
		return $this->redirect(array('action'=>'index'));
	}

	public function deleteComment($id) {
		$this->loadModel('Comment');
		if($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		if($this->Comment->delete($id)) {
			$this->Flash->success(__('The comment has been deleted'));
		} else {
			$this->Flash->success(__('The comment could not been deleted'));
		}
		return $this->redirect(array('action'=>'timeline'));
	}

	public function index() {
		$this->layout = 'user';
		$this->set('authUser', AuthComponent::user('id'));
		$this->paginate = array('limit' => 10);
		$posts = $this->paginate('Post');
		$this->set('posts', $posts);
	}
}
?>
