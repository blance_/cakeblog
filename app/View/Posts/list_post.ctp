<section class="content-header">
	<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">Users</li>
	</ol>
</section>
<?= $this->Html->link('Create Post', array('controller'=>'posts','action' => 'add')) ?><br>
<?php echo $this->start('navigation'); ?>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-users"></i> <span>Users</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href=""><i class="fa fa-circle-o"></i>Administrator</a></li>
			<li><a href=""><i class="fa fa-circle-o"></i>Members</a></li>
		</ul>
	</li>
	<li class="treeview active">
		<a href="#">
			<i class="fa fa-pencil"></i> <span>Posts</span>
		</a>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-calendar"></i> <span>Timeline</span>
		</a>
    </li>
<?php $this->end(); ?>

<section class="content">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">List of Posts</h3>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-striped" id="example1">
				<thead>
					<tr>
						<th>Title</th>
						<th>Body</th>
						<th>Author</th>
						<th>Picture</th>
						<th>Status</th>
						<th>Post Created</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>	
				<?php foreach($posts as $post) : ?>
				<tbody>
					<tr>
						<td><?= $this->Html->link($post['Post']['title'], array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?></td>
						<td><?= $post['Post']['body'];?></td>
						<td><?= $post['User']['username'];?></td>
						<td><?= $post['Post']['post_pic'];?></td>
						<td><?= $post['Post']['status'];?></td>
						<td><?= $post['Post']['created_at'];?></td>
						<td><?= $this->HTML->link('Edit', array('controller'=>'posts', 'action'=>'edit', $post['Post']['id']));?></td>
						<td><?= $this->Form->postlink('Delete', array('controller'=>'posts', 'action'=>'delete', $post['Post']['id']), array('confirm'=>'Are you sure you want to delete this post?'));?></td>
					</tr>
				</tbody>
				<?php endforeach ?>
				<?php unset($post); ?>
			</table>
		</div>
	</div>
</section>